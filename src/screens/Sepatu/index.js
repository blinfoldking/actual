import React from 'react';
import './Sepatu.css'

import Desc from './Desc'
import Input from './Input'
import Show from './Show'
import Detail from './Detail'

export default class Sepatu extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            route: "desc"
        }
    }


    routing(key) {
        console.log(`route to ${key}`)
        const route = {
            "desc": () =>
                <Desc />,
            "input": () =>
                <Input />,
            "show": () =>
                <Show subroute={this.state.subroute} showDetail={() => this.setState({ route: "detail" })} />,
            "detail": () => <Detail />
        }

        return route[key] || (() => undefined);
    }

    render() {
        return (
            <div>
                <nav class="navbar is-info" role="navigation" aria-label="main navigation">
                    <div class="navbar-brand">
                        <a class="navbar-item" href="#">
                            <h1 class="title is-3 has-text-white">LogoDisini</h1>
                        </a>

                        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false"
                            data-target="navbarBasicExample">
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </a>
                    </div>

                    <div id="navbarBasicExample" class="navbar-menu">
                        <div class="navbar-start">
                            <a class="navbar-item">
                                Home
                </a>
                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link">
                                    Products
                    </a>

                                <div class="navbar-dropdown">
                                    <a class="navbar-item">
                                        Shoe
                        </a>
                                    <a class="navbar-item">
                                        Bag
                        </a>
                                    <a class="navbar-item">
                                        Clothing
                        </a>
                                </div>
                            </div>
                        </div>

                        <div class="navbar-end">
                            <div class="navbar-item">
                                <div class="buttons">
                                    <a class="button is-danger">
                                        Log out
                        </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="columns">
                    <aside class="menu shadow column is-2">
                        <p class="menu-label">
                            General
            </p>
                        <ul class="menu-list">
                            <li><a class={this.state.route == "desc" ? "is-active" : ""}
                                onClick={() => this.setState({ route: "desc" })}>Product Detail</a></li>
                            <li><a class={this.state.route == "input" ? "is-active" : ""}
                                onClick={() => this.setState({ route: "input" })}>Input Data</a></li>
                        </ul>
                        <p class="menu-label">
                            Perspectives
            </p>
                        <ul class="menu-list">
                            <li><a onClick={() => this.setState({ route: "show", subroute: "fin" })}>Financial</a></li>
                            <li><a onClick={() => this.setState({ route: "show", subroute: "cust" })}>Customer</a></li>
                            <li><a onClick={() => this.setState({ route: "show", subroute: "int" })}>Internal Process</a></li>
                            <li><a onClick={() => this.setState({ route: "show", subroute: "learn" })}>Learning and Growth</a></li>
                        </ul>
                    </aside>
                    <div class="column">
                        {this.routing(this.state.route)() || <h1>Error</h1>}
                    </div>
                </div>
                <script></script>

            </div>
        )
    }
}