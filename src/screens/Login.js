import React from 'react';
import './Login.css';

export default class Login extends React.Component {
    render() {
        return (
            <div>
                <div class="bg-img"></div>
                <div class="container" id="login" style={{"width": "300px", "top": "30vh", "text-align": "center"}}>
                    <input type="text" class="input" placeholder="Username"/>
                    <input type="password" class="input" placeholder="Password"/>
                    <button class="button is-info" onClick={() => this.props.onLogin()}>Login</button>
                </div> 
            </div>
        )
    }
}