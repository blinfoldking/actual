import React from 'react';

export default class Desc extends React.Component {
    render() {
        return (
            <div>
                <div class="breadcrumb section shadow" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="#">General</a></li>
                        <li class="is-active"><a href="#" aria-current="page">Product Detail</a></li>
                    </ul>
                </div>

                <div class="columns" style={{ padding: "15px" }}>
                    <div class="column is-3 section shadow" style={{ padding: 0 }}>
                        <img 
                            src="https://images.pexels.com/photos/1152077/pexels-photo-1152077.jpeg?cs=srgb&dl=accessory-briefcase-buckle-1152077.jpg&fm=jpg" 
                            alt="" />
                    </div>
                    <div class="column section shadow">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut
                        feugiat lacus. Vestibulum at efficitur lacus, at viverra nibh. Suspendisse eu fermentum magna. Nam
                        id ipsum vel nisi mollis sagittis ultrices sed urna. Phasellus mollis nunc vitae sapien molestie
                        cursus. Sed hendrerit, arcu quis vulputate laoreet, magna metus lacinia tellus, in porttitor nisl
                        libero ut odio. Nulla at mauris non felis volutpat facilisis. In nisl velit, finibus vitae
                    pellentesque a, euismod eu tellus. Nulla facilisi. </div>
                </div>
            </div>
        )
    }
}