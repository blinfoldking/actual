import React from 'react';
import { Doughnut, Line } from 'react-chartjs-2'

const periode = [1999, 2000, 2001, 2002]

const generatedData = periode.map(p => Math.random() * 100)

const lineChart = (
    // A react-chart hyper-responsively and continuusly fills the available
    // space of its parent element automatically
    <div
        style={{
        }}
    >
        <Line
            width="400px"
            height="300px"
            data={{
                labels: periode,
                datasets: [
                    {
                        label: "current",
                        data: generatedData,
                        backgroundColor: "#00555555",
                        borderColor: "#005555"
                    },
                    {
                        label: "baseline",
                        data: periode.map(() => 50),
                        backgroundColor: "#ff003355",
                        borderColor: "#ff0033"
                    },
                    {
                        label: "target",
                        data: periode.map(() => 90),
                        borderColor: "#0055ff"
                    }
                ]
            }}
        />
    </div>
);

const gauge = (
    <div>
        <Doughnut
            width="400px"
            height="300px"
            data={{
                labels: [
                    'Baseline',
                    'Current',
                    'Target'
                ],
                datasets: [{
                    data: [50, 50 - generatedData[0], 10],
                    backgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56'
                    ]
                }]
            }}
            options={{
                rotation: 1 * Math.PI,
                circumference: 1 * Math.PI,
                legend: {
                    // display: false
                },
                tooltips: {
                    enabled: "false"
                }
            }}
        />
        <div class="select">
            <select>
                {periode.map(p => <option>{p}</option>)}
            </select>
        </div>
    </div>
)

export default class Detail extends React.Component {
    render() {
        return (
            <div>
                <div class="breadcrumb section shadow" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="#">General</a></li>
                        <li class="is-active"><a href="#" aria-current="page">KPI Detail</a></li>
                    </ul>
                </div>

                <div class=" section shadow" style={{ padding: 15 }}>
                    <h5 className="title is-">Example KPI</h5>
                </div>
                <div class="columns section shadow" style={{ padding: 30 }}>
                    {lineChart}
                    {gauge}
                </div>
            </div>
        )
    }
}