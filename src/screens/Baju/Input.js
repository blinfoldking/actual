import React from 'react'

export default class Input extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            view: "fin"
        }
    }

    setView(id) {
        console.log("view " + id)
        this.setState({ view: id })
    }

    render() {
        const { view } = this.state
        return <div>

            <div class="breadcrumb section shadow" aria-label="breadcrumbs">
                <ul>
                    <li><a href="#">General</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Input Data</a></li>
                </ul>
            </div>
            <div>
                <div class="section shadow">
                    <div class="tabs is-centered">
                        <ul>
                            <li id="fin" class={view == "fin" ? "is-active" : ""} onClick={() => this.setView("fin")}><a>Financial</a></li>
                            <li id="cust" class={view == "cust" ? "is-active" : ""} onClick={() => this.setView("cust")}><a>Customer</a></li>
                            <li id="int" class={view == "int" ? "is-active" : ""} onClick={() => this.setView("int")}><a>Internal Process</a></li>
                            <li id="learn" class={view == "learn" ? "is-active" : ""} onClick={() => this.setView("learn")}><a>Learning and Growth</a></li>
                        </ul>
                    </div>
                    {
                        view == "fin" &&
                        <div id="financial-input">
                            <h5 class="title is-4">Financial Data</h5>
                            <div class="columns">
                                <div class="column">
                                    <table class="table">
                                        <thead>
                                            <th>Year</th>
                                            <th>Sold</th>
                                            <th>Cost to Plan Make</th>
                                            <th>Direct Material Cost</th>
                                            <th>Indirect Production Cost</th>
                                            <th>Direct Labor Cost</th>
                                            <th>Delivery Cost</th>
                                            <th>Cost to Return</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1999</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                            <tr>
                                                <td>2000</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <strong>New Data :</strong><br />
                                    <input type="text" class="input" placeholder="" style={{ width: "100px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <button class="button is-info">Add</button>
                                </div>
                            </div>
                        </div>
                    }
                    {
                        view == "cust" &&
                        <div id="cust-input">
                            <h5 class="title is-4">Customer Data</h5>
                            <h5 class="title is-5">Yearly Data</h5>
                            <div class="columns">
                                <div class="column">
                                    <table class="table">
                                        <thead>
                                            <th>Year</th>
                                            <th>New Customer</th>
                                            <th>Total Customer</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>January</td>
                                                <td>100</td>
                                                <td>11000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                            <tr>
                                                <td>February</td>
                                                <td>100</td>
                                                <td>11000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <strong>New Data :</strong><br />
                                    <input type="text" class="input" placeholder="" style={{ width: "100px" }} /><br />
                                    <input type="number" class="input" placeholder="New Customer" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="Total Customer" style={{ width: "200px" }} /><br />
                                    <button class="button is-info">Add</button>
                                </div>
                            </div>
                            <h5 class="title is-5">Monthly Data</h5>
                            <div class="columns">
                                <div class="column">
                                    <table class="table">
                                        <thead>
                                            <th>Month</th>
                                            <th>Complain Count</th>
                                            <th>Satisfied Customer</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1999</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                            <tr>
                                                <td>2000</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <strong>New Data :</strong><br />
                                    <input type="text" class="input" placeholder="" style={{ width: "100px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <button class="button is-info">Add</button>
                                </div>
                            </div>
                        </div>
                    }
                    {
                        view == "int" &&
                        <div id="int-input">
                            <h5 class="title is-4">Business Process Data</h5>
                            <h5 class="title is-5">Material Returned Data</h5>
                            <div class="columns">
                                <div class="column">
                                    <table class="table">
                                        <thead>
                                            <th>Month</th>
                                            <th>Shipped On Time</th>
                                            <th>Total</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>January</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                            <tr>
                                                <td>February</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <strong>New Data :</strong><br />
                                    <input type="text" class="input" placeholder="" style={{ width: "100px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <button class="button is-info">Add</button>
                                </div>
                            </div>
                            <h5 class="title is-5">Product Returned Data</h5>
                            <div class="columns">
                                <div class="column">
                                    <table class="table">
                                        <thead>
                                            <th>Month</th>
                                            <th>Shipped On Time</th>
                                            <th>Total</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>January</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                            <tr>
                                                <td>February</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <strong>New Data :</strong><br />
                                    <input type="text" class="input" placeholder="" style={{ width: "100px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <button class="button is-info">Add</button>
                                </div>
                            </div>
                            <h5 class="title is-5">Material Quality Data</h5>
                            <div class="columns">
                                <div class="column">
                                    <table class="table">
                                        <thead>
                                            <th>Month</th>
                                            <th>Good</th>
                                            <th>Damaged</th>
                                            <th>Total</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>January</td>
                                                <td>100</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                            <tr>
                                                <td>February</td>
                                                <td>100</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <strong>New Data :</strong><br />
                                    <input type="text" class="input" placeholder="" style={{ width: "100px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <button class="button is-info">Add</button>
                                </div>
                            </div>
                            <h5 class="title is-5">Product Quality Data</h5>
                            <div class="columns">
                                <div class="column">
                                    <table class="table">
                                        <thead>
                                            <th>Month</th>
                                            <th>Good</th>
                                            <th>Damaged</th>
                                            <th>Total</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>January</td>
                                                <td>100</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                            <tr>
                                                <td>February</td>
                                                <td>100</td>
                                                <td>100</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <strong>New Data :</strong><br />
                                    <input type="text" class="input" placeholder="" style={{ width: "100px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <button class="button is-info">Add</button>
                                </div>
                            </div>
                            <h5 class="title is-5">Stakeholder Data</h5>
                            <div class="columns">
                                <div class="column">
                                    <table class="table">
                                        <thead>
                                            <th>Month</th>
                                            <th>Total Product Recieved</th>
                                            <th>Product Recieved on Time</th>
                                            <th>Product Content Recieved Correct</th>
                                            <th>Total Material Recieved</th>
                                            <th>Material Recieved on Time</th>
                                            <th>Material Content Recieved Correct</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>January</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                            <tr>
                                                <td>February</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td>10000</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <strong>New Data :</strong><br />
                                    <input type="text" class="input" placeholder="" style={{ width: "100px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <button class="button is-info">Add</button>
                                </div>
                            </div>
                        </div>
                    }
                    {
                        view == "learn" &&
                        <div id="learn-input">
                            <h5 class="title is-4">Learning and Growth Data</h5>
                            <h5 class="title is-5">Annual Data</h5>
                            <div class="columns">
                                <div class="column">
                                    <table class="table">
                                        <thead>
                                            <th>Year</th>
                                            <th>Training Done</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1999</td>
                                                <td>100</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                            <tr>
                                                <td>2000</td>
                                                <td>100</td>
                                                <td><button class="button is-danger">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="column">
                                    <strong>New Data :</strong><br />
                                    <input type="text" class="input" placeholder="" style={{ width: "100px" }} /><br />
                                    <input type="number" class="input" placeholder="" style={{ width: "200px" }} /><br />
                                    <button class="button is-info">Add</button>
                                </div>
                            </div>

                        </div>
                    }
                </div>
            </div>

        </div>
    }
}