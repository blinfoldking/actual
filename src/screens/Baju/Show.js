import React from 'react';


const data = {
    "fin": [{
        title: "Increase Revenue",
        kpi: [{
            title: "Sales growth",
            baseline: "90%",
            target: "100%"
        }]
    }, {
        title: "Decrease Cost",
        kpi: [{
            title: "Order Delivery/Install Cost",
            baseline: "10%",
            target: "100%"
        }]
    }],
    "cust": [{
        title: "Increase Number of Customer",
        kpi: [{
            title: "%Customer Acqusition",
            baseline: "50%",
            target: "100%"
        }]
    }, {
        title: "Maintain Customer Loyalty",
        kpi: [{
            title: "%Customer Retention",
            baseline: "90%",
            target: "100%"
        }]
    }, {
        title: "Maintain Customer Satisfaction",
        kpi: [{
            title: "Number of Complain",
            baseline: "1%%",
            target: "0%%"
        }]
    }],
    "int": [{
        title: "Maintain Service Performance",
        kpi: [{
            title: "Returnment Shipped on Time (Material)",
            baseline: "90%",
            target: "100%"
        }, {
            title: "Returnment Shipped on Time (Product)",
            baseline: "90%",
            target: "100%"
        }]
    }, {
        title: "Product Quality Development",
        kpi: [{
            title: "Product Quality(Material)",
            baseline: "90%",
            target: "100%"
        }, {
            title: "Product Quality(Product)",
            baseline: "90%",
            target: "100%"
        }]
    }],
    "learn": [{
        title: "Improve Employees Skill",
        kpi: [{
            title: "Provide Training",
            baseline: "1",
            target: "2"
        }]
    }, {
        title: "Grow Professionalism",
        kpi: [{
            title: "Employee Attendance",
            baseline: "90%",
            target: "100%"
        }]
    }],
}

export default class Show extends React.Component {
    render() {
        return (
            <div>
                <div>
                    <div class="breadcrumb section shadow" aria-label="breadcrumbs">
                        <ul>
                            <li><a href="#">General</a></li>
                            <li class="is-active"><a href="#" aria-current="page">Product Detail</a></li>
                        </ul>
                    </div>

                    <div class="column section shadow">
                        <h5 class="title is-5">Financial Strategies</h5>
                        <div class="columns">
                            <button class="button is-primary"> Add </button>
                        </div>
                    </div>
                    <div class="column section shadow">
                        {
                            data[this.props.subroute].map(strategy => (
                                <div>
                                    <h6 class="title is-6">{strategy.title}</h6>
                                    <div className="columns">
                                        <button className="button is-danger">Delete</button>
                                        <button className="button is-primary">Add KPI</button>
                                    </div>

                                    <table className="table is-fullwidth">
                                        <thead>
                                            <tr>
                                                <th>KPI</th>
                                                <th>baseline</th>
                                                <th>target</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {

                                                strategy.kpi.map(k => (
                                                    <tr>
                                                        <td>{k.title}</td>
                                                        <td>{k.baseline}</td>
                                                        <td>{k.target}</td>
                                                        <td><button className="button is-info" onClick={() => this.props.showDetail()}>Show</button></td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        )
    }
}