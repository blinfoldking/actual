import React from 'react';
import './App.css';


import Login from './screens/Login'
import Select from './screens/Select'


import Sepatu from './screens/Sepatu'
import Tas from './screens/Tas'
import Baju from './screens/Baju'


class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      route: "login"
    }
  }

  render() {
    return (
      <div className="App">
        {this.routing(this.state.route)() || <h1>Error</h1>}
      </div>
    );
  }


  routing(key) {
    console.log(`route to ${key}`)
    const route = {
      "login": () =>
        <Login onLogin={() => this.setState({route: "select"})}/>,
      "select": () => 
        <Select routing={dst => this.setState({route: dst})}/>,
      "sepatu": () =>
        <Sepatu/>,
        "baju": () =>
        <Baju/>,
        "tas": () =>
        <Tas/>
    }

    return route[key];
  }
}

export default App;
